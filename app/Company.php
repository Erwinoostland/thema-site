<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	 protected $fillable = [
      'name', 'address', 'postal', 'city', 'country', 'phone', 'mail', 'userID',
    ];

    public function user(){
        return $this->belongsTo("App\User", "userID", "id");
    }
}
