<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Thema;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Http\File;



class ArticlemanagementController extends Controller
{
    
    public function index() {
    	$articles = Article::all();
    	$countArticles = Article::all()->count();
        $themas = Thema::all();
    	return view('cms/articlemanagement', compact("articles", "countArticles", "themas"));
    }

    public function show($id) {

        $article = Article::find($id);

    	return view('cms/article', compact('article'));
    }

    public function sortByDate() {
    	$articles = Article::orderBy('created_at', 'asc')->get();
    	$countArticles = Article::all()->count();
        $themas = Thema::all();
    	return view('cms/articlemanagement', compact("articles", "countArticles", "themas"));
    }

    public function sortByTitle() {
    	$articles = Article::orderBy('title', 'asc')->get();
    	$countArticles = Article::all()->count();
        $themas = Thema::all();
    	return view('cms/articlemanagement', compact("articles", "countArticles", "themas"));
    }

    // Store new article in DB
    public function store(Request $request) {

    	//Set a new article
    	$newArticle = new Article();

    	//Get all Data
        $newArticle->themaID    = $request->input('theme');
    	$newArticle->title 		= $request->input('title');
    	$newArticle->body 		= $request->input('body');
        
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('images/articles/');

            $request->file('image')->move($destinationPath, $name);

            $newArticle->imageURL = $name;
        }

    	$newArticle->userID 	    = \Auth::id();

    	//Save new article in Database
    	$newArticle->save();

    	//Return back to overview
    	return back();

    	
    }

    public function edit(Request $request, $id) {

        $editArticle = Article::find($id);

        $editArticle->title = $request->input('article-title');
        $editArticle->body = $request->input('article-text');

        // If there is a new image selected
        if($request->hasFile('image')) {

            //Delete the old image
            if (!empty($editArticle->imageURL)) {      
                unlink(public_path('images/articles/' . $editArticle->imageURL));
            }

            // Store the new image
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('images/articles/');

            $request->file('image')->move($destinationPath, $name);

            $editArticle->imageURL = $name;
        }

        $editArticle->update();

        return back();

    }
}