<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\User;

class CompanymanagementController extends Controller
{
    public function index()
    {
        $users = User::all();
        $companies = Company::all();
        $countCompanies = Company::count();

        return view('cms/companymanagement', compact("companies", "countCompanies", "users"));
    }

    public function show($id)
    {
        $users = User::all();
        $company = Company::find($id);
        return view('cms/company', compact('company', 'users'));
    }

    public function delete($id)
    {
        Company::find($id)->delete();
        return back();
    }

    public function store(Request $request) {

        $this->validate(request(),[
            'name' => 'required|max:50|min:2',
            'address' => 'required|max:100|min:2',
            'postal' => 'required|',
            'city' => 'required|max:100|min:2',
            'country' => 'required|max:50|min:2',
            'phone' => 'required|max:20|min:8',
            'mail' => 'required|unique:companies|min:8',
            'user' => 'null'
        ]);

        $company = new Company();
        $company->name = $request->input('name');
        $company->address = $request->input('address');
        $company->postal = $request->input('postal');
        $company->city = $request->input('city');
        $company->country = $request->input('country');
        $company->phone = $request->input('phone');
        $company->mail = $request->input('mail');
        $company->userID = $request->input('userID');

        $company->save();

        return back();
    }

    public function edit(Request $request, $id)
    {

        $editCompany = Company::find($id);
        $editCompany->name = $request->input('name');
        $editCompany->address = $request->input('address');
        $editCompany->city = $request->input('city');
        $editCompany->postal = $request->input('postal');
        $editCompany->phone = $request->input('phone');
        $editCompany->mail = $request->input('mail');
        $editCompany->country = $request->input('country');
        $editCompany->userID = $request->input('userID');


        $editCompany->update();

        return back();
    }

}
