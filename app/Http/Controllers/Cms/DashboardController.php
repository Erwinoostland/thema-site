<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use  App\Article;
use App\Link;
use App\Thema;

class DashboardController extends Controller
{

    public function index()
    {

    	//Get al users from DB
        $users = User::limit(5)->orderBy('created_at', 'DESC')->get();
        $countUsers = User::count();

        //Get all companies from DB
        $companies = Company::limit(5)->orderBy('created_at', 'DESC')->get();
        $countCompanies = Company::count();

        // Get all articles from DB
        $articles = Article::limit(5)->orderBy('created_at', 'DESC')->get();
        $countArticles = Article::count();

        //Get all links from DB
        $links = Link::limit(5)->orderBy('created_at', 'DESC')->get();
        $countLinks = Link::count();

        //Get all themes from DB
        $themes = Thema::limit(5)->orderBy('created_at', 'DESC')->get();
        $countThemes = Thema::count();

        return view('cms/dashboard', 
        		compact(['users', 'countUsers', 'companies', 'countCompanies', 'articles', 'countArticles', 'links', 'countLinks', 'themes', 'countThemes']));
    }
}
