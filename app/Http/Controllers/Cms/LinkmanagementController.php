<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Link;
use App\User;
use App\Thema;
use App\Status;

class LinkmanagementController extends Controller
{
    public function index() {

    	$links = Link::all();
        $countLinks = Link::all()->count();
    	$users = User::all();
    	$themas = Thema::all();
    	$statuses = Status::all();
    	return view('cms/linkmanagement', compact('links', 'users', 'themas', 'statuses', 'countLinks'));
    }

    public function sortByName() {
    	$links = Link::orderBy('linkName', 'ASC')->get();
    	$users = User::all();
    	$themas = Thema::all();
    	$statuses = Status::all();
    	return view('cms/linkmanagement', compact('links', 'users', 'themas', 'statuses'));
    }

    public function show($id) {
        $link = Link::find($id);
        $users = User::all();
        $statuses = Status::all();
        $themas = Thema::all();

        return view('cms/link', compact('link', 'users', 'themas', 'statuses'));
    }

    public function store(Request $request) {

    	$newLink = new Link();
    	$newLink->linkName = $request->input('linkName');
    	$newLink->linkURL = $request->input('linkURL');
    	$newLink->userID = $request->input('owner');
    	$newLink->themeID = $request->input('themaID');
    	$newLink->statusID = $request->input('statusID');

    	$newLink->save();

    	return back();
    }

    public function delete($id) {

        $deleteLink = Link::find($id);

        $deleteLink->delete();

        return redirect('cms/linkbeheer');
    }

    public function edit(Request $request, $id) {
        $editLink = Link::find($id);

        $editLink->linkName = $request->input('linkname');
        $editLink->linkURL = $request->input('linkurl');
        $editLink->userID = $request->input('userID');
        $editLink->themeID = $request->input('themaID');
        $editLink->statusID = $request->input('statusID');

        $editLink->update();

        return back();

    }
}
