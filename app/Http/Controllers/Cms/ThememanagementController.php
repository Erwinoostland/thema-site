<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Thema;

class ThememanagementController extends Controller
{
     public function index() {
    	$themes = Thema::all();
    	$countThemes = Thema::all()->count();

    	 return view('cms/thememanagement', compact('themes', 'countThemes'));
    }

     public function sortByDate() {
    	$themes = Thema::orderBy('created_at', 'asc')->get();
    	$countThemes = Thema::all()->count();
    	return view('cms/thememanagement', compact("themes", "countThemes"));
    }

    public function sortByTitle() {
    	$themes = Thema::orderBy('title', 'asc')->get();
    	$countThemes = Thema::all()->count();
    	return view('cms/thememanagement', compact("themes", "countThemes"));
    }

    public function store(Request $request) {
    	$newTheme = new Thema();
    	$newTheme->title = $request->input('title');
    	$newTheme->description = $request->input('body');

    	if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('images/themes/');

            $request->file('image')->move($destinationPath, $name);

            $newTheme->imageURL = $name;
        }

    	//Save new theme in Database
    	$newTheme->save();

    	return back();
    }

    public function edit(Request $request, $id) {

    	$editTheme = Thema::find($id);
    	$editTheme->title = $request->input('title');
    	$editTheme->description = $request->input('body');

    	// If there is a new image selected
        if($request->hasFile('image')) {

            //Delete the old image
            if (!empty($editTheme->imageURL)) {      
                unlink(public_path('images/themes/' . $editTheme->imageURL));
            }

            // Store the new image
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('images/themes/');

            $request->file('image')->move($destinationPath, $name);

            $editTheme->imageURL = $name;
        }

        $editTheme->update();

        return back();
    }

    public function show($id) {
    	$theme = Thema::find($id);
    	return view('cms/theme', compact('theme'));
    }
}
