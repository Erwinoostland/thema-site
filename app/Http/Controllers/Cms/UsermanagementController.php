<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsermanagementController extends Controller
{
    public function index() {

    	$users = User::all();
    	$countUsers = User::count();

    	return view('cms/usermanagement', compact(['users', 'countUsers']));
    }

    public function delete(User $user) {
    	User::find($user->id)->delete();
    	return back();
    }

    public function makeAdmin(User $user) {
    	User::where('id', $user->id)->update(['roleID' => 2]);
    	return back();
    }

    public function makeUser(User $user) {
    	User::where('id', $user->id)->update(['roleID' => 1]);
    	return back();
    }

    public function show(User $user) {
    	return view('cms/user', compact('user'));
    }

    public function store(Request $request) {

        $this->validate(request(), [
            'firstname' => 'required|max:50|min:2',
            'lastname' => 'required|max:50|min:2',
            'email' => 'required|unique:users|max:150|min:2',
            'password' => 'required|confirmed|min:5'
        ]);

        $user = new User;
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));

        // Store user in Database
        $user->save();

        return back();
    }

    public function edit(Request $request, User $user) {

        $firstname    =   $request->input('firstname');
        $lastname     =   $request->input('lastname');
        $email        =   $request->input('email');

        if (User::where('id', $user->id)->update(['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email])) {
            $sessionMessage = 'Gebruiker is succesvol gewijzigd!';
            $sessionClass = 'alert alert-success';
        } else {
            $sessionMessage = 'Er is iets misgegaan, probeer het opnieuw!';
            $sessionClass = 'alert alert-danger';
        }
        

        return back()->with(['sessionMessage' => $sessionMessage, 'sessionClass' => $sessionClass]);
    }

}
