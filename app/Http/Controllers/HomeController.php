<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

use App\Thema;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themas = Thema::all();
        return view('home', compact('themas'));
    }
}
