<?php

namespace App\Http\Controllers;

use App\Link;
use App\Thema;
use Illuminate\Http\Request;

class LinkbuyController extends Controller
{
    public function index(){
        $themes = Thema::all();
        return view('linkbuy', compact('themes'));
    }

    public function store(Request $request){

        $newLink = new Link();

        $newLink->linkName = $request->input('linkName');
        $newLink->linkURL = $request->input('linkUrl');
        $newLink->themeID = $request->input('themeID');
        $newLink->statusID = 1;

        $newLink->userID = \Auth::id();

        $newLink->save();

        return back();



    }
}
