<?php

namespace App\Http\Controllers;
use App\User;
use App\Company;


use Illuminate\Http\Request;
class ProfileController extends Controller
{
    public function index(){
        $user = User::find(\Auth::id());
        return view('profile', compact('user'));
    }

    public function store(Request $request) {

        $newCompany = new Company();

        $newCompany->name = $request->input('add-company-name');
        $newCompany->userID = \Auth::id();
        $newCompany->address = $request->input('add-company-adres');
        $newCompany->city = $request->input('add-company-place');
        $newCompany->postal = $request->input('add-company-postal');
        $newCompany->country = $request->input('add-company-country');
        $newCompany->phone = $request->input('add-company-phonenumber');
        $newCompany->mail = $request->input('add-company-mail');

        $newCompany->userID = \Auth::id();

        $newCompany->save();

        return back();
    }

    public function saveEdit(Request $request, $id) {
        $editPerson = User::find($id);

        $editPerson->firstname = $request->input('firstname');
        $editPerson->lastname  = $request->input('lastname');
        $editPerson->email    = $request->input('email');
        $editPerson->password = bcrypt($request->input('password'));

        $editPerson->update();
        return back();
    }

    public  function saveEditCompany(Request $request, $id) {

        $editCompany = Company::find($id);

        $editCompany->name = $request->input('companyname');
        $editCompany->address = $request->input('adres');
        $editCompany->postal = $request->input('postal');
        $editCompany->city = $request->input('place');
        $editCompany->country = $request->input('country');
        $editCompany->phone = $request->input('phonenumber');
        $editCompany->mail = $request->input('company-mail');

        $editCompany->update();
        return back();

    }

}
