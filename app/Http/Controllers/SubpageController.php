<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use App\Thema;
use App\Article;

class SubpageController extends Controller
{
    //
    public function index ($id) {
        $links = Link::where("themeID", $id)->get();
        $articles = Article::where("themaID", $id)->get();
        $thema = Thema::find($id);
        return view('subpage', compact('thema', 'links', 'articles'));
    }
}
