<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function thema(){
     return $this->belongsTo("App\Thema", "themeID", "id");
    }
    public function status() {
        return $this->hasOne("App\Status", "id", "statusID");
    }
    public function user() {
        return $this->hasOne("App\User", "id", "userID");
    }
}
