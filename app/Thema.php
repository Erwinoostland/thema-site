<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thema extends Model
{
	public function articles() {
		return $this->hasMany("App\Article", "themaID", "id");
	}
	public function links() {
	    return $this->hasMany("App\Link", "themeID", "id");
    }
}
