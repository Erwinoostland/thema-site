fetch('http://api.openweathermap.org/data/2.5/weather?q=Groningen&APPID=daea4e2bd13fd91da013c2fe4eae8d58&units=metric&lang=nl')
    .then(res => res.json())
    .then(data => {
        function degree() {
            if (data["wind"]["deg"] < 45) {
                return 'Noord-oost';
            }
            else if (data["wind"]["deg"] <= 90) {
                return 'Oost';
            }
            else if (data["wind"]["deg"] <= 135) {
                return 'Zuid-oost'
            }
            else if (data["wind"]["deg"] <= 180) {
                return 'Zuid'
            }
            else if (data["wind"]["deg"] <= 225) {
                return 'Zuid-west'
            }
            else if (data["wind"]["deg"] <= 270) {
                return 'West'
            }
            else if (data["wind"]["deg"] <= 315) {
                return 'Noord-west'
            }
            else if (data["wind"]["deg"] <= 360) {
                return 'Noord'
            }
            return 'Noord'
        }

        let direction = degree();
        document.getElementById('weather').innerHTML = `
                    <h4 class="weather-table__header">Het weer in: ${data.name}</h4>
                    <section class="row">
                    <section class="col-md-12">
                      <table class="weather-table">
                        <tbody>
                        <tr>
                            <td><span class="weather-table__subject">Weer:</span style="text-align: right;"> <img class="weather-table__icon" src="http://openweathermap.org/img/w/${data["weather"]["0"]["icon"]}.png"><span></span><hr></td>
                        </tr>
                         <tr>
                             <td><span class="weather-table__subject">Beschrijving:</span> <span class="weather-table__answers">${data["weather"]["0"]["description"]}</span><hr></td>
                        </tr>
                         <tr>
                             <td><span class="weather-table__subject">Wind:</span> ${data["wind"]["speed"]} Bft<hr></td>
                        </tr>
                           <tr>
                             <td><span class="weather-table__subject">Wind richting:</span> ${direction}<hr></td>
                        </tr>
                        <tr>
                            <td><span class="weather-table__subject">Temperatuur:</span><span class="text-right"> ${data["main"]["temp"]} °C </span><hr> </td>
                        </tr>
                         <tr>
                             <td><span class="weather-table__subject">Luchtvochtigheid:</span> ${data["main"]["humidity"]}%<hr></td>
                        </tr>
                        <tr>
                             <td><span class="weather-table__subject">Luchtdruk:</span> ${data["main"]["pressure"]} hPa<hr></td>
                        </tr>
                        <tr>
                               <td><input placeholder="Plaatsnaam" id="weatherInput" class="weather-table__input" type="text"><button class="weather-table__button" onclick="">Send</button></td>
                        </tr>
                    </tbody>
                    </table>
                   </section>
                  </section>
                `;
    })
    .catch(err => console.warn(err));