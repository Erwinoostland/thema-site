@extends('layouts.app')

@section('content')

    <section class="container">

        <section class="row">
            <section class="col-md-12">
                <h2 class="mt-2">Artikel naam</h2>
                <hr>
            </section>
        </section>
        <section class="row">
            <section class="col-md-5 mt-3">
                <img class="article-img" src="../img/login-background.jpg">
            </section>
            <section class="col-md-7 mt-3">
                <section class="card articledetails-card">
                    <section class="card-body">
                        <h3>Titel van artikel</h3>
                        <hr />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam aliquid aperiam consequatur culpa deserunt error, fugit iusto magnam magni minima mollitia nostrum quasi repellendus sint temporibus tenetur veniam voluptatem.</p>
                    </section>
                </section>
            </section>
        </section>
    </section>







@endsection