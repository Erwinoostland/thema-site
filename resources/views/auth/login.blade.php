@extends('layouts.app')



@section('content')
    <section class="login-screen">
        <section class="login-screen__image">
            <img src="https://www.convident.nl/wp-content/uploads/2015/02/convident-logo.png">
            {{--<section class="login-screen__image--text">--}}
            {{--<h1>Login bij uw account</h1>--}}
            {{--</section>--}}
        </section>

        <section class="login-screen__content">
            <section class="login-screen__content--wrapper">
                <section class="login-form">
                    <h1>Login</h1>
                </section>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <section class="login-form">
                        <input id="email" type="email" placeholder="TYPE UW E-MAIL ADRES    "
                               class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </section>
                    <section class="login-form">
                        <input id="password" type="password" placeholder="TYPE UW WACHTWOORD"
                               class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </section>
                    <section class="login-form">
                        <section class="checkbox">
                            <label>
                                <input type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('BLIJF INGELOGD') }}
                            </label>
                        </section>
                    </section>

                    <section class="login-form">
                        <button type="submit" class="login-form__button--raised">
                            {{ __('Login') }}
                        </button>
                        <a href="/register" type="button" class="login-form__button--flat">Registreren</a>
                    </section>
                    <section class="login-form">
                        <a class="login-form__button--flat--align" href="{{ route('password.request') }}">
                            {{ __('Wachtwoord vergeten?') }}
                        </a>
                    </section>
                </form>
            </section>
        </section>
    </section>
@endsection
