@extends('layouts.app')

@section('content')
    <section class="login-screen">
        <section class="login-screen__image">
            <img src="https://www.convident.nl/wp-content/uploads/2015/02/convident-logo.png">
            {{--<section class="login-screen__image--text">--}}
            {{--<h1>Login bij uw account</h1>--}}
            {{--</section>--}}
        </section>

        <section class="login-screen__content">
            <section class="login-screen__content--wrapper">
                <section class="login-form">
                    <h1>Registreren</h1>
                </section>

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <section class="login-form">

                        <input type="text" placeholder="VOORNAAM"
                               class="login-form__input{{ $errors->has('firstname') ? ' is-invalid' : '' }}"
                               name="firstname" value="{{ old('firstname') }}">

                        @if ($errors->has('firstname'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                        @endif
                    </section>

                    <section class="login-form">
                        <input type="text" placeholder="ACHTERNAAM"
                               class="login-form__input{{ $errors->has('lastname') ? ' is-invalid' : '' }}"
                               name="lastname" value="{{ old('lastname') }}">

                        @if ($errors->has('lastname'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                        @endif
                    </section>

                    <section class="login-form">
                        <input id="email" placeholder="E-MAIL ADRES" type="email"
                               class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </section>

                    <section class="login-form">
                        <input id="password" placeholder="WACHTWOORD" type="password"
                               class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </section>

                    <section class="login-form">
                        <input id="password-confirm" placeholder="WACHTWOORD CONTROLE" type="password"
                               class="login-form__input"
                               name="password_confirmation">
                    </section>

                    <section class="login-form">
                        <a href="../" type="submit" class="login-form__button--flat">
                            <span class="ion-chevron-left"> </span>
                            Terug
                        </a>
                        <button type="submit" class="login-form__button--raised">
                            Registreren
                        </button>
                    </section>
                </form>
            </section>
        </section>
    </section>
@endsection
