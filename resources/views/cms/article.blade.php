@extends('layouts.cms')

@section('content')

<section class="container">
	<section class="row">
		<section class="col-md-12">
			<section class="card">
				<section class="card-header"><h3 class="white-text">Artikel Wijzigen<a href="{{url('/cms/artikelbeheer')}}"><i class="material-icons home-button">home</i></a></h3></section>
				<section class="card-body">

						<p>Hier kunnen alle gegevens van de artikel aangepast worden.</p>

						<hr />					

						<form method="post" action="{{ url('/cms/artikelbeheer/edit/'. $article->id) }}" enctype="multipart/form-data">

							{{ csrf_field() }}
							<section class="row">
							<section class="col-md-6">
								{{-- Article Title --}}
								<label for="article-title">Artikel Titel:</label>
								<input type="text" id="article-title" name="article-title" value="{{ $article->title }}">
								<br />

								{{-- Article Text --}}
								<label for="article-text">Artikel Tekst:</label>
								<textarea name="article-text" id="article-text" class="article-text__textarea">{{ $article->body }}</textarea>
							</section>

							<section class="col-md-6">
								<label for="current-image">Huidige Afbeelding:</label>
								<img src="{{ URL::to('/') }}/images/articles/{{$article->imageURL}}" width="500" height="350">
							</section>

							
							<section class="col-md-6">
								
							</section>

							{{-- Article Image --}}
							<section class="col-md-6">
								<label for="article-new-image">Artikel Afbeelding:</label>
								<input type="file" name="image" id="article-new-image">
							</section>
							</section>

							{{-- Submit button --}}
							<section class="col-md-12">
								<button type="submit" name="submit" class="basic-submit__button basic-button">Opslaan</button>
							</section>

						</form>

				</section>
			</section>
		</section>
	</section>
</section>
@endsection