@extends('layouts.cms')

@section('content')

<section class="container">
	<section class="row">
		<section class="col-md-12">
			<section class="card">
				<section class="card-header">
					<h4 class="white-text">Artikel Beheer<a href="{{url('/cms')}}"><i class="material-icons home-button">home</i></a></h4>
				</section>
				<section class="card-body">
					<p>Aantal artikelen: <span>{{ $countArticles }}</span></p>

					<hr />

					<section class="row">
						<section class="col-md-12 mt-2 mb-2">
							<a href="" data-target="#add-article-modal" data-toggle="modal" class="create-article__button">Nieuwe artikel</a>
							<a href="{{ url('cms/artikelbeheer/date') }}" class="top-article__button">Sorteren op aanmaakdatum</a>
							<a href="{{ url('cms/artikelbeheer/title') }}" class="top-article__button">Sorteren op titel</a>
						</section>
					</section>

					<section class="row">

						{{-- One Card --}}
						@foreach ($articles as $article)
							<section class="col-md-4">
								<section class="card">
									<section class="card-header"><h5 class="white-text">{{ $article->title }}</h5 class="white-text"></section>
									<section class="card-body">
										<section class="card__article">
											<p>
												 {{ str_limit($article->body, 150) }}
											</p>
										</section>
										<a href="{{ url('cms/artikelbeheer/show/' . $article->id)}}" class="basic-button">Wijzig</a>
									</section>
								</section>
							</section>
						@endforeach

				</section>
			</section>
		</section>
	</section>
</section>


{{-- ======================================================================================//
							Add Article Modal
//====================================================================================== --}}
<section class="modal fade" id="add-article-modal" tabindex="-1" role="dialog" aria-labelledby="add-article-modal" aria-hidden="true">
  <section class="modal-dialog" role="document">
    <section class="modal-content">
      <section class="modal-header">
        <h5 class="modal-title" id="add-article-modal">Artikel Toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </section>
      <form method="post" action="{{ url('/cms/artikelbeheer/store') }}" enctype="multipart/form-data">
      		{{ csrf_field() }}
	      <section class="modal-body">

	      	{{-- Article Title --}}
	      	<label for="title">Titel:
	      		<input type="text" class="article-add__input" name="title" id="title">
	      	</label>

	      	{{-- Article Body --}}
	      	<label for="body">Inhoud:
	      		<textarea class="article-text__textarea" name="body" id="body"></textarea>
	      	</label>

	      	{{-- Theme --}}
	      	<label for="theme">
	      		<select id="theme" class="article-add__input" name="theme">
	      			<option value="">Kies een thema...</option>
	      			@foreach ($themas as $thema)
	      				<option value="{{ $thema->id }}">{{ $thema->title}}</option>
	      			@endforeach
	      		</select>
	      	</label>

	      	{{-- Article Image --}}
	      	<label for="image">Afbeelding Kiezen:<br/>
	      		<input type="file" name="image" class="article-add__input" id="image">
	      	</label>

	      </section>
	      <section class="modal-footer	">
	        <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
	        <button type="submit" class="basic-button">Toevoegen</a>
	      </section>
	  </form>
    </section>
  </section>
</section>
@endsection