@extends ('layouts.cms')

@section ('content')

    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="card">
                    <section class="card-header">
                        <h4 class="white-text">Bedrijfs Profiel
                            <a href="{{url('/cms/bedrijfbeheer')}}"><i class="material-icons home-button">home</i></a>
                        </h4></section>
                    <section class="card-body">

                        <p>Hier kun je de bedrijfsgegevens aanpassen.</p>

                        <hr/>

                        <form method="post" action="{{ url('cms/bedrijfbeheer/edit/'. $company->id) }}">

                            {{ csrf_field() }}

                            <section class="row">
                                {{-- Company Name --}}
                                <section class="col-md-6">
                                    <label for="name">Bedrijfsnaam:
                                        <input type="text" name="name" id="name" value="{{ $company->name }}">
                                    </label>
                                </section>

                                {{-- Company Address --}}
                                <section class="col-md-6">
                                    <label for="address">Adres:
                                        <input type="text" name="address" id="address" value="{{ $company->address }}">
                                    </label>
                                </section>

                                {{-- Company Place --}}
                                <section class="col-md-6">
                                    <label for="place">Plaats:
                                        <input type="text" name="city" id="city" value="{{ $company->city }}">
                                    </label>
                                </section>

                                {{-- Company Postal --}}
                                <section class="col-md-6">
                                    <label for="postal">Postcode:
                                        <input type="text" name="postal" id="postal" value="{{ $company->postal }}">
                                    </label>
                                </section>


                                {{-- Company Phone --}}
                                <section class="col-md-6">
                                    <label for="phone">Telefoonnummer:
                                        <input type="number" name="phone" id="phone" value="{{ $company->phone }}">
                                    </label>
                                </section>

                                {{-- Company Email --}}
                                <section class="col-md-6">
                                    <label for="email">Email:
                                        <input type="email" name="mail" id="mail" value="{{ $company->mail }}">
                                    </label>
                                </section>

                                {{-- Company Country --}}
                                <section class="col-md-6">
                                    <label for="country">Land:
                                        <input type="text" name="country" id="country" value="{{ $company->country }}">
                                    </label>
                                </section>

                                {{-- User --}}
                                <section class="col-md-6">
                                    <label for="userID">Gebruiker
                                        <select name="userID" id="userID">
                                            <option value="{{ $company->user['id'] }}">{{$company->user['firstname'] . ' ' . $company->user['lastname'] }}</option>

                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->firstname . $user->lastname }}</option>
                                            @endforeach

                                        </select>
                                    </label>
                                </section>
                            </section>

                            {{-- Submit Button --}}
                            <button type="submit" name="submit" class="save-edit-company__button">Opslaan</button>

                        </form>

                    </section>
                </section>
            </section>
        </section>
    </section>

@endsection