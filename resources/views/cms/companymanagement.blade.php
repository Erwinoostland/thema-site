@extends('layouts.cms')

@section('content')

    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="card">
                    <section class="card-header">
                        <h4 class="white-text user-h4-title">Bedrijven Beheer<a href="{{url('/cms')}}"><i
                                        class="material-icons home-button">home</i></a></h4>
                    </section>
                    <section class="card-body">

                        <p>Aantal bedrijven: <i class="float-number-company__count">{{$countCompanies}}</i></p>

                        <section class="company-manage__buttons mb-3">
                            <a href="" class="company-manage-add__button" data-toggle="modal"
                               data-target="#add-company-modal">Bedrijf Toevoegen</a>
                        </section>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th><a href="">BedrijfID</a></th>
                                <th><a href="">Bedrijfsnaam</a></th>
                                <th>Adres</th>
                                <th>Plaats</th>
                                <th>Telefoon</th>
                                <th>Gebruiker</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td>#{{$company->id }}</td>
                                    <td>{{$company->name }}</td>
                                    <td>{{$company->address}}</td>
                                    <td>{{$company->city}}</td>
                                    <td>{{$company->phone}}</td>
                                    <td>{{$company->user->firstname . ' ' . $company->user->lastname }}</td>
                                    <td>
                                        <a href="{{url('/cms/bedrijfbeheer/'. $company->id)}}"><i
                                                    class="material-icons edit-company__icon">mode_edit</i></a>
                                        <a href="" data-toggle="modal" data-target="#delete-company-modal"><i
                                                    class="material-icons delete-company__icon">delete</i></a>
                                    </td>
                                </tr>


                                {{-- ======================================================================================//
                                Delete company confirmation Modal
                                //====================================================================================== --}}
                                <section class="modal fade" id="delete-company-modal" tabindex="-1" role="dialog"
                                         aria-labelledby="delete-company-modal" aria-hidden="true">
                                    <section class="modal-dialog" role="document">
                                        <section class="modal-content">
                                            <section class="modal-header">
                                                <h5 class="modal-title" id="delete-company-modal">Bedrijf
                                                    verwijderen</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </section>
                                            <section class="modal-body">
                                                <p>Weet je zeker dat je dit bedrijf wilt verwijderen?</p>
                                            </section>
                                            <section class="modal-footer">
                                                <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
                                                <a href="{{url('cms/bedrijfbeheer/' . $company->id . '/delete')}}"
                                                   class="company-manage__button company-delete__button">Verwijderen</a>
                                            </section>
                                        </section>
                                    </section>
                                </section>
                            @endforeach
                            </tbody>
                        </table>
                    </section>
                </section>
            </section>
        </section>
    </section>

    {{-- ======================================================================================//
                                Add company Modal
    //====================================================================================== --}}
    <section class="modal fade" id="add-company-modal" tabindex="-1" role="dialog" aria-labelledby="add-company-modal"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="add-company-modal">Bedrijf Toevoegen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>

                <form method="post" action="{{url('cms/bedrijfbeheer/store')}}">

                    {{csrf_field()}}

                    <section class="modal-body">

                        {{-- companyName --}}
                        <label for="name">Bedrijfsnaam:
                            <input type="text" name="name" id="name" value="">
                        </label>

                        {{-- Address --}}
                        <label for="address">Adres:
                            <input type="text" name="address" id="address" value="">
                        </label>

                        {{-- Postal--}}
                        <label for="postal">Postcode:
                            <input type="text" name="postal" id="postal" value="">
                        </label>

                        {{-- Place --}}
                        <label for="city">Plaats:
                            <input type="text" name="city" id="place" value="">
                        </label>

                        {{-- Country --}}
                        <label for="country">Land:
                            <input type="text" name="country" id="country" value="">
                        </label>

                        {{-- Phone --}}
                        <label for="phone">Telefoonnummer:
                            <input type="number" name="phone" id="phone" value="">
                        </label>

                        {{-- companyMail --}}
                        <label for="email">Email:
                            <input type="email" name="mail" id="email" value="">
                        </label>

                        <label for="userID">Eigenaar van bedrijf:
                            <select id="userID" name="userID">
                                <option value="">Kies één gebruiker</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->firstname . $user->lastname }}</option>
                                @endforeach
                            </select>
                        </label>

                    </section>

                    <section class="modal-footer	">
                        <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
                        <button type="submit" name="submit" class="basic-button">Toevoegen</button>
                    </section>
                </form>

            </section>
        </section>
    </section>
@endsection