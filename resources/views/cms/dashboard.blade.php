@extends ('layouts.cms')

@section ('content')

    <section class="container-fluid ">
        <section class="row">

            {{--UserManagement Card--}}
            <section class="col-md-5 offset-1">
                <section class="card">
                    <section class="card-header">
                        <h5 class="white-text">User beheer</h5>
                    </section>
                    <section class="card-body">
                        <p>Aantal users: <span>{{ $countUsers }}</span></p>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Voornaam</th>
                                <th>Achternaam</th>
                                <th>Email</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->lastname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td><a href="{{url('cms/userbeheer/' . $user->id)}}"><i class="material-icons">&#xE150;</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{--All users Button--}}
                        <a href="{{url('cms/userbeheer')}}" class="basic-button">Beheer</a>

                    </section>
                </section>
            </section>

            {{--Articles Management Card--}}
            <section class="col-md-5">
                <section class="card">
                    <section class="card-header">
                        <h5 class="white-text">Artikel Beheer</h5>
                    </section>
                    <section class="card-body">
                        <p>Aantal artikelen: <span>{{ $countArticles }}</span></p>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Titel</th>
                                <th>Aangemaakt op</th>
                                <th>Door</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($articles as $article)
                                <tr>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ date('d-m-Y H:i', strtotime($article->created_at)) }}</td>
                                    <td>{{ $article->user->firstname . ' ' . $article->user->lastname }}</td>
                                    <td><a href="{{url('cms/artikelbeheer/show/' . $article->id)}}"><i class="material-icons">&#xE150;</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{--All Articles--}}
                        <a href="{{url('/cms/artikelbeheer')}}" class="basic-button">Beheer</a>

                    </section>
                </section>
            </section>
        </section>

        <section class="row">
            {{--Link Management Card--}}
            <section class="col-md-5 offset-1">
                <section class="card">
                    <section class="card-header">
                        <h5 class="white-text">Link beheer</h5>
                    </section>
                    <section class="card-body">
                        <p>Aantal links: <span>{{ $countLinks }}</span></p>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Link naam</th>
                                <th>Link URL</th>
                                <th>Eigenaar</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($links as $link)
                                <tr>
                                    <td>{{ $link->linkName }}</td>
                                    <td>{{ $link->linkURL }}</td>
                                    <td>{{ $link->user->firstname . ' ' . $link->user->lastname }}</td>
                                    <td>{{ $link->status->title }}</td>
                                    <td><a href="{{url('/cms/linkbeheer/show/'. $link->id)}}"><i class="material-icons">&#xE150;</i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        {{--All Links--}}
                        <a href="{{url('/cms/linkbeheer')}}" class="basic-button">Beheer</a>
                    </section>
                </section>
            </section>

            {{--company Management Card--}}
            <section class="col-md-5">
                <section class="card">
                    <section class="card-header">
                        <h5 class="white-text">Thema beheer</h5>
                    </section>
                        <section class="card-body">
                        <p>Aantal Themas: <span>{{$countThemes }}</span></p>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Titel</th>
                                <th>Omschrijving</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($themes as $theme)
                                <tr>
                                    <td>{{ $theme->title }}</td>
                                    <td>{{ $theme->description}}</td>
                                    <td><a href="{{url('/cms/themabeheer/'. $theme->id)}}"><i
                                                    class="material-icons">&#xE150;</i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{--All companies--}}
                        <a href="{{url('cms/themabeheer/')}}" class="basic-button">Beheer</a>
                    </section>
                </section>
            </section>

            {{--company Management Card--}}
            <section class="col-md-10 offset-1">
                <section class="card">
                    <section class="card-header">
                        <h5 class="white-text">Bedrijf Beheer</h5>
                    </section>
                    <section class="card-body">
                        <p>Aantal Bedrijven: <span>{{$countCompanies }}</span></p>

                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Bedrijfsnaam</th>
                                <th>Adres</th>
                                <th>Plaats</th>
                                <th>Postcode</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($companies as $company)
                                <tr>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->address}}</td>
                                    <td>{{ $company->city }}</td>
                                    <td>{{ $company->postal }}</td>
                                    <td><a href="{{url('/cms/bedrijfbeheer/'. $company->id)}}"><i
                                                    class="material-icons">&#xE150;</i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{--All companies--}}
                        <a href="{{url('cms/bedrijfbeheer')}}" class="basic-button">Beheer</a>
                    </section>
                </section>
            </section>
        </section>
    </section>

@endsection