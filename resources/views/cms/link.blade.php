@extends ('layouts.cms')

@section ('content')


    <section class="container">
        <section class="row link-center__flexbox">
            <section class="col-md-6">
                <section class="card">
                    <section class="card-header"><h3 class="white-text"><a href="{{url('/cms')}}"><i
                                        class="material-icons home-button">home</i></a></h3></section>
                    <section class="card-body">
                        <p>Hier kunnen alle gegevens van de link aangepast worden.</p>

                        <hr/>

                        <form method="post" action="{{ url('cms/linkbeheer/edit/' . $link->id) }}">
                            {{ csrf_field() }}

                        {{--Input field Linkname--}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="linkname">Linknaam:
                                    <input type="text" id="linkname" name="linkname" value="{{ $link->linkName }}">
                                </label>
                            </section>
                        </section>

                        {{--Input field Link URL--}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="linkurl">Link-URL:
                                    <input type="text" id="linkurl" name="linkurl" value="{{ $link->linkURL }}">
                                </label>
                            </section>
                        </section>

                        {{--Input field Company--}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="userID">Eigenaar:
                                    <select name="userID" id="userID">
                                        <option value="{{$link->userID}}">{{ $link->user->firstname . ' ' . $link->user->lastname}}</option>
                                        @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->firstname .' ' . $user->lastname}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                        </section>

                        {{-- Thema --}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="themaID">Thema:
                                <select name="themaID" id="themaID">
                                      <option value="{{ $link->themeID }}">{{ $link->thema->title }}</option>
                                      @foreach($themas as $thema)
                                        <option value="{{$thema->id}}">{{$thema->title}}</option>
                                      @endforeach
                                </select>
                                </label>
                            </section>
                        </section>

                        {{--Input field Status--}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="statusID">Status:
                                    <select name="statusID" id="statusID">
                                        <option value="{{ $link->statusID }}">{{ $link->status->title }}</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}">{{ $status->title }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </section>
                        </section>

                        {{-- Submit button --}}
                        <section class="col-md-12">
                            <button type="submit" name="submit" class="basic-button">Opslaan</button>
                            <a href="" class="basic-button delete__button" data-toggle="modal" data-target="#delete-link-modal">Verwijder</a>
                        </section>
                    </form>
                    </section>
                </section>
            </section>
        </section>
    </section>

     {{-- ======================================================================================//
                                  Delete link confirmation Modal
    //====================================================================================== --}}
    <section class="modal fade" id="delete-link-modal" tabindex="-1" role="dialog" aria-labelledby="delete-link-modal" aria-hidden="true">
        <section class="modal-dialog" role="document">
        <section class="modal-content">
            <section class="modal-header">
            <h5 class="modal-title" id="delete-link-modal">Link verwijderen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </section>

            <form method="post" action="{{ url('cms/linkbeheer/delete/'. $link->id) }}">
                {{ csrf_field() }}
                <section class="modal-body">
                    <p>Weet je zeker dat deze link wilt verwijderen?</p>
                </section>
                <section class="modal-footer">
                <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
                <button type="submit" class="user-manage__button basic-button user-delete__button">Verwijderen</a>
                </section>
            </form>

        </section>
        </section>
    </section>
@endsection