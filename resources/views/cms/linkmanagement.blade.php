@extends('layouts.cms')

@section('content')

<section class="container">
	<section class="col-md-12">
		<section class="card">
			<section class="card-header"><h4 class="white-text">Link Beheer<a href="{{url('/cms')}}"><i class="material-icons home-button">home</i></a></h4></section>
			<section class="card-body">
				<p>Aantal Links: <i class="float-number__usercount">{{ $countLinks }}</i></p>

						<section class="user-manage__buttons mb-3">
							<a href="" class="user-manage-add__button" data-toggle="modal" 	data-target="#add-link-modal">Link Toevoegen</a>
						</section>
						
						<table class="table table-striped table-responsive">
							<thead>
								<tr>
									<th><a href="{{ url('cms/linkbeheer/')}}">LinkID</a></th>
									<th><a href="{{ url('cms/linkbeheer/sorteer')}}">Link naam</a></th>
									<th>Link URL</th>
									<th>Toegevoegd door:</th>
									<th></th>

								</tr>
							</thead>
							<tbody>
                  @foreach ($links as $link)
    								<tr>
    									<td>{{ $link->id }}</td>
    									<td>{{ $link->linkName }}</td>
    									<td><a href="{{ $link->linkURL }}" target="_blank">{{ $link->linkURL }}</a></td>
    									<td>{{ $link->user->firstname . ' ' . $link->user->lastname }}</td>
    									<td>
                        <a href="{{ url('/cms/linkbeheer/show/'.$link->id) }}"><i class="material-icons edit-user__icon">mode_edit</i></a>
    									</td>
    								</tr>

                  @endforeach
							</tbody>
						</table>
			</section>
		</section>
	</section>
</section>

{{-- ======================================================================================//
							Add Link Modal
//====================================================================================== --}}
<section class="modal fade" id="add-link-modal" tabindex="-1" role="dialog" aria-labelledby="add-link-modal" aria-hidden="true">
  <section class="modal-dialog" role="document">
    <section class="modal-content">
      <section class="modal-header">
        <h5 class="modal-title" id="delete-link-modal">Link Toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </section>

      <form method="post" action="{{ url('cms/linkbeheer/store') }}">

      {{ csrf_field() }}
      <section class="modal-body">

      	{{-- Link Name --}}
      	<label for="linkname">Link Naam:
      		<input type="text" name="linkName" id="linkname" value="">
      	</label>

      	{{-- Link URL --}}
      	<label for="linkUrl">Link URL:
      		<input type="text" name="linkURL" id="linkUrl" value="">
      	</label>

        {{-- Thema --}}
        <label for="themaID">Thema:
        <select name="themaID" id="themaID">
          <option value="">Kies een thema...</option>
          @foreach($themas as $thema)
            <option value="{{$thema->id}}">{{$thema->title}}</option>
          @endforeach
        </select>
        </label>

        {{-- Status --}}
        <label for="statusID">Status:
        <select name="statusID" id="statusID">
          <option value="">Kies een status...</option>
          @foreach($statuses as $status)
            <option value="{{$status->id}}">{{$status->title}}</option>
          @endforeach
        </select>
        </label>

      	{{-- Link Owner --}}
      	<label for="owner">Eigenaar:
      		<select name="owner">
      			<option value="">Kies een gebruiker...</option>
              @foreach($users as $user)
        			   <option value="{{ $user->id }}">{{ $user->firstname . ' ' . $user->lastname }}</option>

              @endforeach
      		</select>
        </label>

      </section>
      <section class="modal-footer">
        <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
        <button type="submit" class="basic-button">Toevoegen</button>
      </section>
    </form>
    </section>
  </section>
</section>


@endsection