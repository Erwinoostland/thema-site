@extends('layouts.cms')

@section('content')

<section class="container">
	<section class="row">
		<section class="col-md-12">
			<section class="card">
				<section class="card-header"><h3 class="white-text">Thema Wijzigen<a href="{{url('/cms/themabeheer')}}"><i class="material-icons home-button">home</i></a></h3></section>
				<section class="card-body">

						<p>Hier kunnen alle gegevens van de thema aangepast worden.</p>

						<hr />					

						<form method="post" action="{{ url('/cms/themabeheer/edit/'. $theme->id) }}" enctype="multipart/form-data">

							{{ csrf_field() }}
							<section class="row">
							<section class="col-md-6">
								{{-- Article Title --}}
								<label for="theme-title">Thema Titel:</label>
								<input type="text" id="theme-title" name="title" value="{{ $theme->title }}">
								<br />

								{{-- Article Text --}}
								<label for="theme-text">Thema Tekst:</label>
								<textarea name="body" id="theme-text" class="article-text__textarea">{{ $theme->description }}</textarea>
							</section>

							<section class="col-md-6">
								<label for="current-image">Huidige Afbeelding:</label>
								<img src="{{ URL::to('/') }}/images/themes/{{$theme->imageURL}}" width="500" height="350">
							</section>

							
							<section class="col-md-6">
								
							</section>

							{{-- Article Image --}}
							<section class="col-md-6">
								<label for="theme-new-image">Artikel Afbeelding:</label>
								<input type="file" name="image" id="theme-new-image">
							</section>
							</section>

							{{-- Submit button --}}
							<section class="col-md-12">
								<button type="submit" name="submit" class="basic-submit__button basic-button">Opslaan</button>
							</section>

						</form>

				</section>
			</section>
		</section>
	</section>
</section>
@endsection