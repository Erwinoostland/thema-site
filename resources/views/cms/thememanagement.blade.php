@extends('layouts.cms')

@section('content')

<section class="container">
	<section class="row">
		<section class="col-md-12">
			<section class="card">
				<section class="card-header">
					<h4 class="white-text">Thema Beheer<a href="{{url('/cms')}}"><i class="material-icons home-button">home</i></a></h4>
				</section>
				<section class="card-body">
					<p>Aantal themas: <span>{{ $countThemes }}</span></p>

					<hr />

					<section class="row">
						<section class="col-md-12 mt-2 mb-2">
							<a href="" data-target="#add-theme-modal" data-toggle="modal" class="create-article__button">Nieuwe Thema</a>
							<a href="{{ url('cms/themabeheer/date') }}" class="top-article__button">Sorteren op aanmaakdatum</a>
							<a href="{{ url('cms/themabeheer/title') }}" class="top-article__button">Sorteren op titel</a>
						</section>
					</section>

					<section class="row">

						{{-- One Card --}}
						@foreach ($themes as $theme)
							<section class="col-md-4">
								<section class="card">
									<section class="card-header"><h5 class="white-text">{{ $theme->title }}</h5 class="white-text"></section>
									<section class="card-body">
										<section class="card__article">
											<p>
												 {{ str_limit($theme->description, 150) }}
											</p>
										</section>
										<a href="{{ url('cms/themabeheer/show/' . $theme->id)}}" class="basic-button">Wijzig</a>
									</section>
								</section>
							</section>
						@endforeach

				</section>
			</section>
		</section>
	</section>
</section>


{{-- ======================================================================================//
							Add Theme Modal
//====================================================================================== --}}
<section class="modal fade" id="add-theme-modal" tabindex="-1" role="dialog" aria-labelledby="add-theme-modal" aria-hidden="true">
  <section class="modal-dialog" role="document">
    <section class="modal-content">
      <section class="modal-header">
        <h5 class="modal-title" id="add-article-modal">Thema Toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </section>
      <form method="post" action="{{ url('/cms/themabeheer/store') }}" enctype="multipart/form-data">
      		{{ csrf_field() }}
	      <section class="modal-body">

	      	{{-- Theme Title --}}
	      	<label for="title">Titel:
	      		<input type="text" class="article-add__input" name="title" id="title">
	      	</label>

	      	{{-- Theme Body --}}
	      	<label for="body">Inhoud:
	      		<textarea class="article-text__textarea" name="body" id="body"></textarea>
	      	</label>

	      	{{-- Theme Image --}}
	      	<label for="image">Afbeelding Kiezen:<br/>
	      		<input type="file" name="image" class="article-add__input" id="image">
	      	</label>

	      </section>
	      <section class="modal-footer	">
	        <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
	        <button type="submit" class="basic-button">Toevoegen</a>
	      </section>
	  </form>
    </section>
  </section>
</section>
@endsection