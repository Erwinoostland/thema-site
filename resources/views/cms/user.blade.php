@extends('layouts.cms')

@section('content')

	<section class="container">
		<section class="row">

			@if(\Session::has('sessionMessage'))
				<section class="col-md-12">
					<section class="{{ \Session::get('sessionClass') }}">{{ \Session::get('sessionMessage') }}</section>
				</section>
			@endif

			{{-- Personal Information --}}
			<section class="col-md-6">
				<section class="card edit-user__card">
					<section class="card-header">
						<h5 class="white-text">Persoonlijke gegevens
							<a href="{{url('/cms/userbeheer')}}"><i class="material-icons home-button">home</i></a>
						</h5>
					</section>
					<section class="card-body">

						<p>Hier kunnen alle persoonlijke gegevens gewijzigd worden.</p>

						<hr />

						{{-- Edit user form --}}
						<form method="post" action="{{ url('/cms/userbeheer/store/' . $user->id )}}">

							{{ csrf_field() }}

							{{-- Firstname --}}
							<section class="row">
								<section class="col-md-12">
									<label for="firstname">Voornaam:
										<input type="text" id="firstname" name="firstname" class="edit-user-input" value="{{$user->firstname}}">
									</label>
								</section>
							</section>

							{{-- Lastname --}}
							<section class="row">
								<section class="col-md-12">
									<label for="lastname">Achternaam:
										<input type="text" id="lastname" name="lastname" class="edit-user-input" value="{{$user->lastname}}">
									</label>
								</section>
							</section>

							{{-- Email --}}
							<section class="row">
								<section class="col-md-12">
									<label for="email">Email:
										<input type="email" id="email" name="email" class="edit-user-input" value="{{$user->email}}">
									</label>
								</section>
							</section>

							{{-- New Password --}}
							<section class="row">
								<section class="col-md-12">
									<label for="password">Nieuw Wachtwoord:
										<input type="password" id="password" name="password" class="edit-user-input" value="">
									</label>
								</section>
							</section>

							{{-- Confirmation Password --}}
							<section class="row">
								<section class="col-md-12">
									<label for="password-confirm">Heraal Nieuw Wachtwoord:
										<input type="password" id="password-confirm" class="edit-user-input" name="password_confirmation">
									</label>
								</section>
							</section>

							{{-- Submit Button --}}
							<button type="submit" name="submit" class="submit-edit-user__button">Opslaan</button>
						</form>
					</section>
				</section>
			</section>

			{{-- Company Informatino --}}
			<section class="col-md-6">
				<section class="card edit-user__card">
					<section class="card-header">
						<h5 class="white-text">Bedrijfs gegevens
							<a href="{{url('/cms/userbeheer')}}"><i class="material-icons home-button">home</i></a>
						</h5>
					</section>
					<section class="card-body">

						<p>Hier staan de gegevens van het gekoppelde bedrijf.</p>

						<hr />

						{{-- Edit user form --}}
						<form method="post" action="{{$user->id}}/edit">

							{{-- Companyname --}}
							<section class="row">
								<section class="col-md-12">
									<label for="companyname">Bedrijfsnaam:
										<input type="text" id="companyname" name="companyname" class="edit-user-input" value="" disabled>
									</label>
								</section>
							</section>

							{{-- Adress --}}
							<section class="row">
								<section class="col-md-8">
									<label for="adress">Adres:
										<input type="text" id="adress" name="adress" class="edit-user-input" value="" disabled>
									</label>
								</section>

								{{-- Postal --}}
								<section class="col-md-4">
									<label for="postal">Postcode:
										<input type="text" id="postal" name="postal" class="edit-user-input" value="" disabled>
									</label>
								</section>
							</section>

							{{-- City --}}
							<section class="row">
								<section class="col-md-12">							
									<label for="city">Plaats:
										<input type="text" id="city" name="city" class="edit-user-input" value="" disabled>
									</label>
								</section>
							</section>

							{{-- Phone --}}
							<section class="row">
								<section class="col-md-12">							
									<label for="city">Telefoonnummer:
										<input type="number" id="phone" name="phone" class="edit-user-input" value="" disabled>
									</label>
								</section>
							</section>

							{{-- Email --}}
							<section class="row">
								<section class="col-md-12">							
									<label for="email">Email:
										<input type="email" id="email" name="email" class="edit-user-input" value="" disabled>
									</label>
								</section>	
							</section>				

							{{-- Edit Company Button --}}
							<section class="row">
								<section class="col-md-12">				
										<a  href="{{url('cms/bedrijfbeheer/find')}}" class="edit-company-from-user__button">Bewerken</a>
								</section>	
							</section>				
						</form>
					</section>
				</section>
			</section>
		</section>
	</section>

@endsection