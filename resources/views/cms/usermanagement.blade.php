@extends('layouts.cms')

@section('content')

	<section class="container">
		<section class="row">
			<section class="col-md-12">
				<section class="card">
					<section class="card-header">
						<h4 class="white-text user-h4-title">Gebruiker Beheer<a href="{{url('/cms')}}"><i class="material-icons home-button">home</i></a></h4>
					</section>
					<section class="card-body">

						<p>Aantal gebruikers: <i class="float-number__usercount">{{$countUsers}}</i></p>

						<section class="user-manage__buttons mb-3">
							<a href="" class="user-manage-add__button" data-toggle="modal" 	data-target="#add-user-modal">Gebruiker Toevoegen</a>
						</section>

						<table class="table table-striped table-responsive">
							<thead>
								<tr>
									<th>GebruikerID</th>
									<th>Voornaam</th>
									<th><a href="" class="">Achternaam</a></th>
									<th>Email</th>
									<th><a href="" class="">Bedrijfsnaam</a></th>
									<th>Rol</th>
									<th></th>

								</tr>
							</thead>
							<tbody>
							{{-- Foreach loop --}}
							@foreach($users as $user)
								<tr>
									<td>#{{$user->id}}</td>
									<td>{{$user->firstname}}</td>
									<td>{{$user->lastname}}</td>
									<td>{{$user->email}}</td>
									<td>("Bedrijfsnaam")</td>
									<td>{{$user->roleID}}</td>
									<td>
										{{-- If user is a admininstrator --}}
										@if ($user->roleID == 2)
											<a href="{{url('cms/userbeheer/' . $user->id . '/make-user')}}"><i class="material-icons give-user__icon">arrow_downward</i></a>

										{{-- If user is a user --}}
										@elseif($user->roleID == 1)
											<a href="{{url('cms/userbeheer/' . $user->id . '/make-admin')}}"><i class="material-icons give-admin__icon">arrow_upward</i></a>
										@endif

										<a href="{{url('cms/userbeheer/' . $user->id . '')}}"><i class="material-icons edit-user__icon">mode_edit</i></a>
										<a href="" data-toggle="modal" data-target="#delete-user-modal"><i class="material-icons delete-user__icon">delete</i></a>
									</td>
								</tr>

                                {{-- ======================================================================================//
							    Delete user confirmation Modal
                                //====================================================================================== --}}
                                <section class="modal fade" id="delete-user-modal" tabindex="-1" role="dialog" aria-labelledby="delete-user-modal" aria-hidden="true">
                                    <section class="modal-dialog" role="document">
                                        <section class="modal-content">
                                            <section class="modal-header">
                                                <h5 class="modal-title" id="delete-user-modal">Gebruiker verwijderen</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </section>
                                            <section class="modal-body">
                                                <p>Weet je zeker dat je dit gebruiker wilt verwijderen?</p>
                                            </section>
                                            <section class="modal-footer">
                                                <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
                                                <a href="{{url('cms/userbeheer/' . $user->id . '/delete')}}" class="user-manage__button user-delete__button">Verwijderen</a>
                                            </section>
                                        </section>
                                    </section>
                                </section>
							@endforeach
							</tbody>
						</table>
					</section>
				</section>
			</section>
		</section>
	</section>


{{-- ======================================================================================//
							Add User Modal
//====================================================================================== --}}
<section class="modal fade" id="add-user-modal" tabindex="-1" role="dialog" aria-labelledby="add-user-modal" aria-hidden="true">
  <section class="modal-dialog" role="document">
    <section class="modal-content">
      <section class="modal-header">
        <h5 class="modal-title" id="add-user-modal">Gebruiker Toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </section>
      <form method="post" action="{{url('cms/userbeheer/store')}}">

      	{{ csrf_field() }}

	      <section class="modal-body">

	      	{{-- Fistname --}}
	      	<label for="firstname">Voornaam:
	      		<input type="text" name="firstname" id="firstname" class="add-user__input">
	      	</label>

	      	{{-- Lastname --}}
	      	<label for="lastname">Achternaam:
	      		<input type="text" name="lastname" id="lastname" class="add-user__input">
	      	</label>

	      	{{-- Email --}}
	      	<label for="email">Email:
	      		<input type="email" name="email" id="email" class="add-user__input">
	      	</label>

	      	{{-- Password --}}
	      	<label for="password">Wachtwoord:
	      		<input type="password" name="password" id="password" class="add-user__input">
	      	</label>

	      	{{-- Confirm Password --}}
	      	<label for="confirmation_password">Herhaal Wachtwoord:
	    		<input type="password" name="password_confirmation" id="password_confirmation" class="add-user__input">
	    	</label>

	      </section>
		  <section class="modal-footer">
		    <a href="" class="basic-button" data-dismiss="modal">Sluiten</a>
		    <button type="submit" name="submit" class="basic-button">Toevoegen</button>
		  </section>
      </form>
    </section>
  </section>
</section>

@endsection