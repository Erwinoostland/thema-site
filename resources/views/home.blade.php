@extends('layouts.app')

@section('content')
    <section class="container-fluid">
        <section class="row">
            <section class="col-md-2">
                <section class="paper-card">
                    <div id="weather"></div>
                </section>
            </section>
            <section class="col-md-2">

            </section>
            @foreach($themas as $thema)
                @php
                    $links = \DB::table("links")->where('themeID',$thema->id)->get();
                @endphp
                <section class="col-md-2">
                    <section class="paper-card">
                        <section class="paper-card__image">
                            <img src="{{URL::to('/') }}/images/themes/{{$thema->imageURL}}">
                        </section>
                        <section class="paper-card__title">
                            <h3><a href="{{url("themas/" . $thema->id)}}">{{$thema->title}}</a></h3>
                        </section>
                        @foreach($links as $link)
                            <p class="paper-card__link">
                                <a href="//{{$link->linkURL}}">{{$link->linkName}}</a>
                            </p>
                        @endforeach
                    </section>
                </section>
            @endforeach
        </section>
    </section>
@endsection