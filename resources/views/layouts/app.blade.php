<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/weather.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>

<div id="app">
    @if (!\Request::is('login'))
        @if (!\Request::is('register'))
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a href="{{URL::to('/')}}" class="navbar-brand">
                        <img src="http://www.vastgoedlounge.nl/wp-content/uploads/2015/11/convident-logo-met-tagline3.png">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto navbar-menu">
                            <section class="navbar-search">
                                <input class="navbar-search__input" placeholder="Zoeken">
                                <button class="navbar-search__button"><i class="fas fa-search"></i>

                                </button>
                            </section>
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto navbar-menu">
                            <!-- Authentication Links -->
                            @guest
                                <li><a class="nav-link" href="/login">Inloggen</a></li>
                                <li><a class="nav-link" href="/register">Registreren</a></li>
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->firstname }} <span class="caret"></span>
                                    </a>


                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            <section class="container-fluid">
                <button id="floatingBtn" class="contact-icon" onclick="changeClass">
                    <i id="floatingIcon" class="ion-chatbox"></i>
                </button>
                <section id="contactForm" class="contact-form hidden">
                    <form name="contactForm" id="contactForm">
                        <h3 class="contact-text">Contact</h3>
                        <p class="contact-text">
                            Heb je een vraag?
                            <br>
                            Stur ons dan een berichtje!
                        </p>
                        <input name="contact-fname" class="contact-input" type="text" placeholder="Naam" required>
                        <input name="contact-email" class="contact-input" type="email" placeholder="email" required>
                        <textarea name="contact-ftext" class="contact-input" placeholder="Bericht" required></textarea>
                        <input id="contactButton" class="submit-button" type="submit" value="Verzend"
                               onclick="validateForm()">
                    </form>
                    <script>
                        function changeClass() {
                            const floatingBtn = document.querySelector('floatingBtn');
                            const floatingIcon = document.querySelector('floatingIcon');

                            floatingBtn.addEventListener('click', (e) => {
                                e.preventDefault();
                                if (floatingIcon.classList.contains('ion-chatbox')) {
                                    floatingIcon.classlist.remove('ion-chatbox');
                                    floatingIcon.classList.add('ion-android-cancel')
                                } else {
                                    floatingIcon.classList.remove('ion-android-cancel');
                                    floatingIcon.classlist.add('ion-chatbox');
                                }
                            });
                        }
                    </script>
                </section>
            </section>
        @endif {{-- End of Check for register--}}
    @endif {{-- End of Check for login--}}

    <main class="py-4">
        @yield('content')
    </main>
</div>
@yield('script')
</body>
</html>