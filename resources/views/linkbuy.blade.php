@extends('layouts.app')

@section('content')
    <section class="container">
        <section class="row">
            <section class="col-md-8 offset-2">
                <section class="paper-card">
                    <section class="paper-card__title">
                        <h2 class="text-center">Vraag een link aan</h2>
                    </section>
                    <section class="paper-card__title">
                        <p class="text-center">
                            Hier kunt u een link aanvragen en deze laten plaatsen op onze website.
                        </p>
                    </section>

                    <form method="post" action="{{ url('/linkbuy/store/')}}" class="linkbuy-form">

                        {{ csrf_field() }}
                        <section>
                            <input type="text" name="linkName" placeholder="Linknaam">
                        </section>
                        <section>
                            <input type="text" name="linkUrl" placeholder="LinkURL">
                        </section>
                        <section>
                            <select name="themeID">
                                <option value="">Kies één</option>
                                @foreach ($themes as $theme)
                                    <option value="{{ $theme->id }}">{{$theme->title}}</option>
                                @endforeach
                            </select>
                        </section>
                        <section>
                            <button class="linkbuy-button" type="submit">Betalen</button>
                        </section>

                    </form>
                </section>
            </section>
        </section>
    </section>
@endsection