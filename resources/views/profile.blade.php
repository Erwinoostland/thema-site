@extends('layouts.app')

@section('content')

    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="row">
                    <section class="col-md-6">
                        <section class="card">
                            <section class="card-header">Persoonlijke gegevens</section>
                            <section class="card-body">
                                <table class="table">
                                    <tr>
                                        <td colspan="4">Voornaam:</td>
                                        <td>{{$user->firstname}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Achternaam:</td>
                                        <td>{{$user->lastname}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Email:</td>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Aantal links</td>
                                        <td></td>
                                    </tr>
                                </table>
                                <a href="" data-target="#edit-user-modal" data-toggle="modal"
                                   class="basic-button profile-edit__button">Wijzig</a>
                            </section>
                        </section>
                    </section>
                    <section class="col-md-6">
                        <section class="card">
                            <section class="card-header">Bedrijf gegevens</section>
                            {{--<section class="card-body">--}}
                            <section class="card-body">
                                @if($user->company)
                                    <table class="table">
                                        <tr>
                                            <td colspan="4">Bedrijfsnaam:</td>
                                            <td>{{$user->company->name}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Adres:</td>
                                            <td>{{$user->company->address}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="1">Postcode:</td>
                                            <td>{{$user->company->postal}}</td>
                                            <td>Plaats</td>
                                            <td></td>
                                            <td>{{$user->company->city}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Land:</td>
                                            <td>{{$user->company->country}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Telefoonnummer:</td>
                                            <td>{{$user->company->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Bedrijfs e-mail:</td>
                                            <td>{{$user->company->mail}}</td>
                                        </tr>
                                    </table>
                                    <a href="" data-target="#edit-company-modal" data-toggle="modal"
                                       class="basic-button profile-edit__button">Wijzig</a>
                                @else
                                    <a href="" data-target="#add-company-modal" data-toggle="modal"
                                       class="basic-button add-company__button">Voeg een bedrijf toe</a>
                                @endif
                            </section>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
    {{-- ======================================================================================//
							edit User Modal
//====================================================================================== --}}
    <section class="modal fade" id="edit-user-modal" tabindex="-1" role="dialog" aria-labelledby="edit-user-modal"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="edit-user-modal"> Persoongegevens Wijzigen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                <form method="post" action="{{ url('/profile/store/'. \Auth::id()) }}">

                    {{ csrf_field() }}
                    <section class="modal-body">

                        {{-- Fistname --}}
                        <label for="firstname">Voornaam:
                            <input type="text" name="firstname" id="firstname" class="edit-user__input"
                                   value="{{$user->firstname}}">
                        </label>

                        {{-- Lastname --}}
                        <label for="lastname">Achternaam:
                            <input type="text" name="lastname" id="lastname" class="edit-user__input"
                                   value="{{$user->lastname}}">
                        </label>

                        {{-- Email --}}
                        <label for="email">Email:
                            <input type="email" name="email" id="email" class="edit-user__input"
                                   value="{{$user->email}}">
                        </label>

                        {{-- Password --}}
                        <label for="password">Wachtwoord:
                            <input type="password" name="password" id="password" class="edit-user__input">
                        </label>

                        {{-- Confirm Password --}}
                        <label for="confirmation_password">Herhaal Wachtwoord:
                            <input type="password" name="confirmation_password" id="confirmation_password"
                                   class="edit-user__input">
                        </label>
                    </section>

                    <section class="modal-footer">
                        <button class="basic-button" data-dismiss="modal">Sluiten</button>
                        <button type="submit" class="basic-button">Opslaan</button>
                    </section>
                </form>
            </section>
        </section>
    </section>

    {{-- ======================================================================================//
							edit company Modal
//====================================================================================== --}}
    <section class="modal fade" id="edit-company-modal" tabindex="-1" role="dialog" aria-labelledby="edit-company-modal"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="edit-company-modal">Bedrijf gegevens Wijzigen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                @if($user->company)
                    <form method="post" action="{{ url('/profile/store/company/'. $user->company->id) }}">

                        {{ csrf_field() }}
                        <section class="modal-body">

                            {{-- Company name --}}
                            <label for="companyname">Bedrijfsnaam:
                                <input type="text" name="companyname" id="companyname" class="edit-company__input"
                                       value="{{$user->company->name}}">
                            </label>

                            {{-- adres --}}
                            <label for="adres">Adres
                                <input type="text" name="adres" id="adres" class="edit-company__input"
                                       value="{{$user->company->address}}">
                            </label>

                            {{-- Postal --}}
                            <label for="postal">Postcode:
                                <input type="text" name="postal" id="postal" class="edit-company__input"
                                       value="{{$user->company->postal}}">
                            </label>

                            {{-- Place --}}
                            <label for="place">Plaats:
                                <input type="text" name="place" id="place" class="edit-company__input"
                                       value="{{$user->company->city}}">
                            </label>

                            {{-- Country --}}
                            <label for="country">Land:
                                <input type="text" name="country" id="country" class="edit-company__input"
                                       value="{{$user->company->country}}">
                            </label>

                            {{-- Phone number --}}
                            <label for="phonenumber">Telefoonnummer:
                                <input type="number" name="phonenumber" id="phonenumber" class="edit-company__input"
                                       value="{{$user->company->phone}}">
                            </label>

                            {{-- Company email --}}
                            <label for="company-mail">Bedrijfs email:
                                <input type="email" name="company-mail" id="company-mail" class="edit-company__input"
                                       value="{{$user->company->mail}}">
                            </label>
                        </section>
                        <section class="modal-footer">
                            <button class="basic-button" data-dismiss="modal">Sluiten</button>
                            <button type="submit" class="basic-button">Opslaan</button>
                        </section>
                    </form>
                @endif
            </section>
        </section>
    </section>
    </section>

    {{-- ======================================================================================//
							add company Modal
//====================================================================================== --}}
    <section class="modal fade" id="add-company-modal" tabindex="-1" role="dialog" aria-labelledby="add-company-modal"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="add-company-modal">Bedrijf toevoegen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                <form method="post" action="/profile/store">
                    <section class="modal-body">
                        {{csrf_field()}}

                        {{-- Company name --}}
                        <label for="add-company-name">Bedrijfsnaam:
                            <input type="text" name="add-company-name" id="add-company-name" class="add-company__input">
                        </label>

                        {{-- adres --}}
                        <label for="add-company-adres">Adres
                            <input type="text" name="add-company-adres" id="add-company-adres"
                                   class="add-company__input">
                        </label>

                        {{-- Place --}}
                        <label for="add-company-place">Plaats:
                            <input type="text" name="add-company-place" id="add-company-place"
                                   class="add-company__input">
                        </label>

                        {{-- Postal --}}
                        <label for="add-company-postal">Postcode:
                            <input type="text" name="add-company-postal" id="add-company-postal"
                                   class="add-company__input">
                        </label>

                        {{-- Country --}}
                        <label for="add-company-country">Land:
                            <input type="text" name="add-company-country" id="add-company-country"
                                   class="add-company__input">
                        </label>

                        {{-- Phone number --}}
                        <label for="add-company-phonenumber">Telefoonnummer:
                            <input type="number" name="add-company-phonenumber" id="add-company-phonenumber"
                                   class="add-company__input">
                        </label>

                        {{-- Company email --}}
                        <label for="add-company-mail">Bedrijfs email:
                            <input type="email" name="add-company-mail" id="add-company-mail"
                                   class="add-company__input">
                        </label>
                    </section>
                    <section class="modal-footer">
                        <button class="basic-button" data-dismiss="modal">Sluiten</button>
                        <button type="submit" class="basic-button">Opslaan</button>
                    </section>
                </form>
            </section>
        </section>
    </section>



@endsection