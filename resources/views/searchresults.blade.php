@extends('layouts.app')

@section('content')

    <section class="container">

        <section class="row">
            <section class="col-md-6">
                <h1 class="mt-2">Zoekresultaten</h1>
                <hr>
                <h2>sorteren</h2>
            </section>
        </section>

        <section class="row mb-5">
            <section class="col-md-6">
                <button type="submit" class="sort-button">ABC</button>
                <button type="submit" class="sort-button">Nieuwste</button>
                <button type="submit" class="sort-button">oudste</button>
            </section>
        </section>


        <section class="row">
            <section class="col-md-6">
                <section class="card searchresults-card">
                    <section class="card-body">
                        <h3><a href="">Resultaat 1</a></h3>
                        <hr/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </section>
                </section>
            </section>
        </section>

        <section class="row">
            <section class="col-md-6">
                <section class="card searchresults-card">
                    <section class="card-body">
                        <h3><a href="">Resultaat 2</a></h3>
                        <hr/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque fugit magni
                            minima, molestias, nam nostrum omnis porro possimus quam saepe sapiente sed sit suscipit
                            unde. Accusamus fugiat perferendis rem.</p>
                    </section>
                </section>
            </section>
        </section>

        <section class="row">
            <section class="col-md-6">
                <section class="card searchresults-card">
                    <section class="card-body">
                        <h3><a href="">Resultaat 3</a></h3>
                        <hr/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam architecto aut consectetur
                            cumque, debitis delectus dolore, ducimus esse eveniet in iure mollitia similique unde ut
                            voluptate. Itaque minus natus reiciendis.</p>
                    </section>
                </section>
            </section>
        </section>
    </section>



@endsection