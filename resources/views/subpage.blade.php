@extends('layouts.app')

@section('content')

    <section class="container">
        <a href="{{URL::previous()}}" class="back__button "><i class="fas fa-arrow-alt-circle-left"></i> Terug</a>
        <section class="paper-card">
            <section class="paper-card__image--article">
                <img src="{{URL::to('/') }}/images/themes/{{$thema->imageURL}}">
            </section>
            <h1>{{$thema->title}}</h1>
            <p class="article-paragraph">
            </p>
            <p class="article-paragraph">{{$thema->description}}</p>
            @foreach($links as $link)
                <a href="\\{{$link->linkURL}}" target="_blank" class="article-paragraph">{{$link->linkName}}</a>
            @endforeach
        </section>

        <section class="row">
            @foreach($articles as $article)
                <section class="col-md-3 ">
                    <section class="paper-card">
                        <section class="paper-card__image">
                            <img src="{{URL::to('/') }}/images/articles/{{$article->imageURL}}">
                        </section>
                        <section class="paper-card__title">
                            <h4><a href="{{url("articles/" . $article->id)}}">{{$article->title}}</a></h4>
                        </section>
                    </section>
                </section>
            @endforeach
        </section>
    </section>
@endsection