<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

// Site Routes
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/searchresults', 'SearchresultsController@index');
Route::get('/articledetails', 'ArticledetailsController@index');
Route::get('/themas/{id}', 'SubpageController@index');


Route::group(['prefix' => 'linkbuy'], function () {
    Route::get('/', 'LinkbuyController@index');
    Route::post('/store', 'LinkbuyController@store');
});

Route::group(['prefix' => 'profile'], function () {
   Route::post('/store/{id}', 'ProfileController@saveEdit');
   Route::post('/store/company/{id}', 'ProfileController@saveEditCompany');
    Route::get('/', 'ProfileController@index');
    Route::post('/store', 'ProfileController@store');
});
// CMS Routes
Route::group(['prefix' => 'cms'], function() {

	// Dasboard
	Route::get('/', 'Cms\DashboardController@index')->name('dashboard');

	// Usermanagement
	Route::group(['prefix' => 'userbeheer'], function() {
		Route::get('/', 'Cms\UsermanagementController@index');
		Route::get('/{user}/delete', 'Cms\UsermanagementController@delete');
		Route::get('/{user}/make-admin', 'Cms\UsermanagementController@makeAdmin');
		Route::get('/{user}/make-user', 'Cms\UsermanagementController@makeUser');
		Route::get('/{user}', 'Cms\UsermanagementController@show');
		Route::post('/store', 'Cms\UsermanagementController@store');
		Route::post('/store/{user}', 'Cms\UsermanagementController@edit');
	});//End Prefix => Userbeheer

	Route::group(['prefix' => 'artikelbeheer'], function() {
		Route::get('/', 'Cms\ArticlemanagementController@index');
		Route::get('/show/{id}', 'Cms\ArticlemanagementController@show');
		Route::get('/date', 'Cms\ArticlemanagementController@sortByDate');
		Route::get('/title', 'Cms\ArticlemanagementController@sortByTitle');
		Route::post('/store', 'Cms\ArticlemanagementController@store');
		Route::post('/edit/{id}', 'Cms\ArticlemanagementController@edit');
	});

	Route::group(['prefix' => 'bedrijfbeheer'], function() {
		Route::get('/', 'Cms\CompanymanagementController@index');
		Route::get('/{id}', 'Cms\CompanymanagementController@show');
		Route::get('/{company}/delete', 'Cms\CompanymanagementController@delete');
        Route::post('/edit/{company}', 'Cms\CompanymanagementController@edit');
        Route::post('/store', 'Cms\CompanymanagementController@store');
	});

	Route::group(['prefix' => 'linkbeheer'], function() {
		Route::get('/', 'Cms\LinkmanagementController@index');
		Route::post('/edit/{id}', 'Cms\LinkmanagementController@edit');
		Route::get('/sorteer', 'Cms\LinkmanagementController@sortByName');
		Route::post('/store', 'Cms\LinkmanagementController@store');
		Route::get('/show/{id}', 'Cms\LinkmanagementController@show');
		Route::post('/delete/{id}', 'Cms\LinkmanagementController@delete');
	});


	Route::group(['prefix' => 'themabeheer'], function() {
		Route::get('/', 'Cms\ThememanagementController@index');
		Route::get('/date', 'Cms\ThememanagementController@sortByDate');
		Route::get('/title', 'Cms\ThememanagementController@sortByTitle');
		Route::post('/store', 'Cms\ThememanagementController@store');
		Route::post('/edit/{id}', 'Cms\ThememanagementController@edit');
		Route::get('/show/{id}', 'Cms\ThememanagementController@show');
	});
});//End Prefix => CMS